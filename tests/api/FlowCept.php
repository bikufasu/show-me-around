<?php
$I = new ApiTester($scenario);
$I->wantTo('login as user');
$I->setHeader('X-USER-DEVICEID',"111-111");
$I->sendGET('user');

$I->seeResponseCodeIs(200);
$I->seeResponseIsJson();
$response = json_decode($I->grabResponse());

$I->setHeader('X-SESSIONID',$response->user->session_id);
$I->sendPOST('user/location',['lat' => '41.094527', 'long' => '29.079428']);
$I->seeResponseCodeIs(200);
$I->seeResponseIsJson();

$I->sendGET('location/get_near_users?lat=41.094527&long=29.079428');
$users = json_decode($I->grabResponse());
$I->seeResponseCodeIs(200);
$I->seeResponseIsJson();

// select one user
$user = $users->users[0];

// request event
$I->sendPOST('event/',[
   'to' => $user->session_id,
    'message' => 'hi!'
]);

$I->seeResponseCodeIs(200);
$I->seeResponseIsJson();
$event_rsp = $I->grabResponse();
$evt = json_decode($event_rsp,true);
$event_id = $evt['event']['_id']['$id'];

$I->wantTo('get event details');
$I->sendGET('event/'.$event_id);
$I->seeResponseCodeIs(200);
$I->seeResponseIsJson();
$I->seeResponseContainsJson([41.094527, 29.079428]);
