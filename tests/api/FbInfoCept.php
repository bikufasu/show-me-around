<?php
$I = new ApiTester($scenario);
$I->wantTo('update my info with facebook acc token');
$I->setHeader('X-USER-DEVICEID',"111-111");
$I->sendGET('user');

$I->seeResponseCodeIs(200);
$I->seeResponseIsJson();
$response = json_decode($I->grabResponse());

$I->setHeader('X-SESSIONID',$response->user->session_id);

$I->haveFacebookTestUserAccount();

$I->sendPOST('user/', [
    'access_token' => $I->grabFacebookTestUserAccessToken(),
    'id' => '123'
]);
$I->seeResponseCodeIs(200);
$I->seeResponseIsJson();

$I->sendGET('user');
$I->seeResponseCodeIs(200);
$I->seeResponseIsJson();
$I->seeResponseContains($I->grabFacebookTestUserFirstName());