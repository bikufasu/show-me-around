<?php
/**
 * Created by PhpStorm.
 * User: mvelioglu
 * Date: 02/05/15
 * Time: 18:43
 */

$I = new ApiTester($scenario);
$I->wantTo('login as user');
$I->setHeader('X-USER-DEVICEID', "111-111");
$I->sendGET('user');

$I->seeResponseCodeIs(200);
$I->seeResponseIsJson();
$response = json_decode($I->grabResponse());

$I->wantTo('create new event');
$I->setHeader('X-SESSIONID', $response->user->session_id);
$I->sendPOST('event/', ['to' => $response->user->session_id, 'message' => 'hello']);
$I->seeResponseCodeIs(200);
$I->seeResponseIsJson();
$event_response = json_decode($I->grabResponse(), true);
$eventId = $event_response['event']['_id']['$id'];
$I->wantTo('see created event');
$I->sendGET(sprintf('event/%s', $eventId));
$I->seeResponseCodeIs(200);
$I->seeResponseIsJson();

$I->wantTo('send a message to event');
$I->sendPOST(sprintf('event/%s/message', $eventId), ['to' => $response->user->session_id, 'message' => 'selam dayi']);
$I->seeResponseCodeIs(200);
$I->seeResponseIsJson();