<?php
/**
 * User: engin
 * Date: 21/03/15
 * Time: 10:40
 */

namespace App\Model;


use Silex\Application;
use App\Repos\User as UserRepo;
use App\Repos\Event as EventRepo;
use App\Repos\Location as LocationRepo;

class Provider
{
    private $app;

    public function __construct(Application $app)
    {
        $this->app = $app;

    }

    public function userProvider($id)
    {
        $userObject = new UserRepo($this->app['mongodb']['default']);
        return $userObject->init($id);
    }

    public function LocationProvider($lat, $long)
    {
        $locationObject = new LocationRepo($this->app['mongodb']['default']);
        return $locationObject->init($lat, $long);
    }

    /**
     * @param $id
     * @return EventRepo
     */
    public function eventProvider($id)
    {
        $eventObject = new EventRepo($this->app['mongodb']['default']);
        return $eventObject->init($id);
    }

    public function DeviceProvider($id)
    {
        return new Device($id);
    }
}