<?php
/**
 * Created by PhpStorm.
 * User: engin
 * Date: 21/03/15
 * Time: 10:25
 */

namespace App\Model;


use App\Mongo;

class Base
{

    use Mongo;

    public function save()
    {
        return;
        if ($this->_id != null) {
            $this->get_collection()->update(['_id' => new \MongoId($this->_id)], $this->__ToArray(), ['fsync' => true]);
        } else {
            $this->get_collection()->save($this->__ToArray(), ['fsync' => true]);
        }

    }

    protected function populate($id, $column = '_id')
    {

        $values = $this->get_collection()->findOne([
            $column => $id
        ]);

        if ($values === null) {
            throw new \Exception("Record not found");
        }

        foreach ($this->__ToArray() as $prop => $val) {
            // $val should be null always
            $this->{$prop} = $values[$prop];
        }

        $this->_id = $values['_id'];
    }

    public function test()
    {
        //$test = $this->listDatabases();
        //var_dump($test);
    }

    /**
     * @return \MongoCollection
     */
    protected function get_collection()
    {

        return $this->selectCollection($this->selectDatabase("sma"), $this->collection);
    }

    private function __ToArray($obj = null)
    {
        $obj = ($obj != null) ? $obj : $this;
        $array_vals = get_object_vars($obj);
        $public_properties = (new \ReflectionObject($obj))->getProperties(\ReflectionProperty::IS_PUBLIC);

        $return_array = [];

        foreach ($public_properties as $prop) {
            if (is_object($array_vals[$prop->name]) && ($array_vals[$prop->name] instanceof \DateTime) == false) {
                $return_array[$prop->name] = $this->__ToArray($array_vals[$prop->name]);
            } else {
                $return_array[$prop->name] = $array_vals[$prop->name];
            }
        }

        return $return_array;
    }


}