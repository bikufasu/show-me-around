<?php
/**
 * Project: show-me-around
 * Author: Mehmet Ali Ergut ( memnuniyetsizim )
 * Date: 02/04/15
 * Time: 17:39
 */

namespace App\Model;


class Error extends \Exception
{
    const ERROR_404 = 10000;
    const ERROR_10001 = 10001;

    const MESSAGE_10000 = 'Please contact with administrator';
    const MESSAGE_10001 = 'Co';

    public static function get($errorCode, $extraMessage = null)
    {
        switch ($errorCode) {
            case self::ERROR_404:
                $http_status = 404;
                break;
            default:
                $http_status = 500;
                break;
        }
        $message = self::message($errorCode);
        return array(
            'status' => 'error',
            'code' => $errorCode,
            'message' => $message . $extraMessage,
            'http_status' => $http_status
        );
    }

    public static function message($errorCode)
    {
        $d = constant("App\\Model\\Error::MESSAGE_$errorCode");
        return $d;
    }
}