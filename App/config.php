<?php
/**
 * Project: show-me-around
 * Author: Mehmet Ali Ergut ( memnuniyetsizim )
 * Date: 15/03/15
 * Time: 20:33
 */
header('Access-Control-Allow-Origin: *');

$app = new Silex\Application();
$app['debug'] = true;

$app->register(new Mongo\Silex\Provider\MongoServiceProvider, array(
    'mongo.connections' => array(
        'default' => array(
            'server' => "mongodb://127.0.0.1:27017",
            'options' => array("connect" => true)
        )
    ),
));

// registers validator service for validation given inputs to api resources
$app->register(new Silex\Provider\ValidatorServiceProvider());

\Facebook\FacebookSession::setDefaultApplication("824078594294854", "331d5dc5141e0b742a4b4324633e55de");

include_once __DIR__ . '/routes.php';

return $app;