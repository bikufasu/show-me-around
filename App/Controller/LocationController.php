<?php
/**
 * Created by PhpStorm.
 * User: engin
 * Date: 21/03/15
 * Time: 09:38
 */

namespace App\Controller;

use App\Repos\Location;
use Silex\Application;
use Silex\ControllerProviderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;

class LocationController implements ControllerProviderInterface
{

    private $app;

    public function getNearUsers(Location $location)
    {
        return $location->getNearUsers(1000);
    }

    public function connect(Application $app)
    {
        $this->app = $app;
        $controllers = $app['controllers_factory'];

        $controllers->get('/get_near_users', function (Request $request) use ($app) {

            $location = $app['model.provider']->locationProvider((double) $request->get('lat'), (double) $request->get('long'));
            $users = $this->getNearUsers($location);

            return $app->json([
                'status' => 'ok',
                'users' => $users
            ]);
        });

        return $controllers;
    }
}