<?php
/**
 * Created by PhpStorm.
 * User: engin
 * Date: 21/03/15
 * Time: 09:38
 */

namespace App\Controller;

use Silex\Application;
use Silex\ControllerProviderInterface;
use Symfony\Component\HttpFoundation\Request;
use App\Libs\Util;

class UserController implements ControllerProviderInterface
{

    private $app;

    public function updateLocation($lat, $long)
    {
        $this->app['logged_user']->setLocation($lat, $long);

        $this->app['logged_user']->addUser();
    }

    public function connect(Application $app)
    {
        $this->app = $app;
        $controllers = $app['controllers_factory'];

        $controllers->get('/', function () use ($app) {
            return $app->json($app['logged_user']);
        });

        $controllers->post('/', function (Request $request) use ($app) {
            // only thing to save besides location is social accounts
            $access_token = $request->get('access_token', null);
            $id = $request->get('id', null);
            if (isset($access_token) && isset($id)) {
                $fb_response = Util::getInfoFromFacebook($access_token);
                $user_info = json_decode($fb_response);
                $app['logged_user']->setName($user_info->name);
                $app['logged_user']->setPhotoLink('https://graph.facebook.com/' . $user_info->id . '/picture');

                $fb = new \StdClass();
                $fb->id = $user_info->id;
                $fb->access_token = $access_token;

                $app['logged_user']->setFacebookInfo($fb);
                $app['logged_user']->addUser();
            }
            return $app->json([
                'status' => "ok"
            ]);
        });

        $controllers->post('/location', function () use ($app) {
            $this->updateLocation(
                $app['request']->get('lat', 0),
                $app['request']->get('long', 0)
            );

            return $app->json([
                'status' => "ok"
            ]);
        });

        $controllers->get('/{user}', function ($user) use ($app) {
            return $app->json($user);
        })
            ->convert('user', 'model.provider:userProvider');


        return $controllers;
    }
}