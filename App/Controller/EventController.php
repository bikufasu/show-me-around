<?php
/**
 * Created by PhpStorm.
 * User: engin
 * Date: 21/03/15
 * Time: 09:47
 */

namespace App\Controller;

use App\Repos\Event;
use Silex\Application;
use Silex\ControllerProviderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;
use App\Entity\User;

class EventController implements ControllerProviderInterface
{
    private $app;

    public function updateStatus(Event $event, $status)
    {
        $event->setStatus($status);
        $event->add();
    }

    public function rate(Event $event, Request $request)
    {
        if ($event->getStatus() === "complete") {
            if ($event->getObject()->requester === $this->app['logged_user']) {
                $event->getObject()->requester_rate = $request->get('rate', '');
            } elseif ($event->getObject()->responder == $this->app['logged_user']) {
                $event->getObject()->responder_rate = $request->get('rate', '');
            } else {
                throw new \Exception("You should be in event to rate");
            }

            if ($request->get('comment', '') !== "") {
                $event->addComment($this->app['logged_user'], $request->get('message'));
            }

            $event->add();
        }
    }


    public function getMessages(Event $event)
    {
        return $event->getMessages();
    }

    public function addMessage(Event $event, User $user, $message)
    {
        $event->addMessage($user, $message);
        $event->add();
    }

    public function connect(Application $app)
    {
        $this->app = $app;
        $controllers = $app['controllers_factory'];

        $controllers->post('', function (Request $request) use ($app) {
            // create new Event
            $validationErrors = $app['validator']->validate(
                [
                    'to' => $request->get('to'),
                    'message' => $request->get('message')
                ]
                ,
                new Assert\Collection(
                    [
                        'fields' =>
                            [
                                'to' => new Assert\NotBlank(),
                                'message' => new Assert\NotBlank()
                            ]

                    ]
                ));

            if (0 != count($validationErrors)) {
                foreach ($validationErrors as $validationError) {
                    $errors[] = $validationError->getPropertyPath() . ' ' . $validationError->getMessage() . "\n";
                }

                return $app->json([
                    'status' => false,
                    'errors' => $errors,
                ]);

            }
            $event = $app['model.provider']->eventProvider(null);
            $event->setRequester($app['logged_user']->getObject());
            $event->setResponder($app['model.provider']->userProvider($request->get('to'))->getObject());
            $event->addMessage($app['logged_user']->getObject(), $request->get('message', ''));
            $event->setLocation($app['logged_user']->getLocation());

            $event->add();

            return $app->json([
                'status' => true,
                'event' => $event->getObject()
            ]);
        });

        $controllers->get('/{event}/message', function ($event) use ($app) {
            return $app->json($this->getMessages($event));
        })->convert('event', 'model.provider:eventProvider');

        $controllers->post('/{event}/message', function ($event) use ($app) {
            $message = $app['request']->get('message', '');
            if (trim($message) === "") {
                throw new \Exception("Message can not be empty");
            }
            $this->addMessage($event, $app['logged_user']->getObject(), $message);

            return $app->json([
                'status' => true,
                'event' => $event->getObject()
            ]);
        })->convert('event', 'model.provider:eventProvider');


        $controllers->match('/{event}/{action}', function ($event, $action) use ($app) {
            if ($action === 'approve' || $action === "complete") {
                $this->updatestatus($event, $action);
            } else {
                $this->rate($event, $app['request']);
            }

            return $app->json([
                'status' => true
            ]);

        })
            ->method('POST|GET')
            ->assert('action', 'approve|complete|rate')
            ->convert('event', 'model.provider:eventProvider');

        $controllers->get('/{event}', function ($event) use ($app) {
            return $app->json($event->getObject());
        })->convert('event', 'model.provider:eventProvider');

        return $controllers;
    }
}