<?php
/**
 * Created by PhpStorm.
 * User: mvelioglu
 * Date: 03/04/15
 * Time: 00:20
 */

namespace App\Entity;


class Location
{
    public $lat;
    public $long;

    public function __construct($lat, $long)
    {
        $this->lat = $lat;
        $this->long = $long;
    }


    public function toArray()
    {
        return get_object_vars($this);
    }
}