<?php
/**
 * Created by PhpStorm.
 * User: mvelioglu
 * Date: 03/04/15
 * Time: 00:17
 */

namespace App\Entity;

class User
{
    //public $_id;
    public $name;
    public $photo_link;
    public $location;
    public $social_accounts;
    public $events;
    public $device_id;
    public $session_id;
    public $created_at;
    public $updated_at;

    function __construct($id)
    {
        $this->session_id = $id;
        $this->created_at = new \DateTime('now');
    }

    public function __ToString()
    {
        return $this->name;
    }

    public function toArray()
    {
        return get_object_vars($this);

    }

}