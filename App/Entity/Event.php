<?php
/**
 * Created by PhpStorm.
 * User: engin
 * Date: 18/04/15
 * Time: 10:40
 */

namespace App\Entity;


class Event
{

    public $requester;
    public $responder;
    public $approved = false;
    public $approve_date;
    public $date;
    public $requester_rate;
    public $responder_rate;
    public $status = "requested";
    public $location;

    public $comments = [];
    public $messages = [];

    public function __construct()
    {
        $this->date = new \DateTime('now');
    }

    public function addComment(User $user, $comment)
    {
        $cmt = new \stdClass();
        $cmt->user = $user;
        $cmt->comment = $comment;
        $this->comments[] = $cmt;
    }

    public function addMessage(User $user, $message)
    {
        $msg = new \stdClass();
        $msg->user = $user;
        $msg->message = $message;
        $msg->date = new \DateTime("now");

        $this->messages[] = $msg;
    }

    public function getMessages()
    {
        return $this->messages;
    }
}