<?php
/**
 * Created by PhpStorm.
 * User: engin
 * Date: 19/04/15
 * Time: 15:29
 */

namespace App\Entity;


class Device
{

    public $device_id;
    public $registration_id;
    public $last_used;

    public function __construct($device_id, $registration_id)
    {
        $this->device_id = $device_id;
        $this->registration_id = $registration_id;
        $this->last_used = new \DateTime('now');
    }
}