<?php
/**
 * Created by PhpStorm.
 * User: engin
 * Date: 19/04/15
 * Time: 15:31
 */

namespace App\Repos;

use App\Entity\Device as DeviceEntity;

class Device
{

    private $dbConnector;
    protected $collection = 'user';
    public $entity;


    public function  __construct($dbConnectionClient)
    {
        $this->dbConnector = $dbConnectionClient;
        $this->dbConnector = $this->dbConnector->selectCollection('sma', $this->collection);
    }

    public function init($device_id, $registration_id)
    {
        if ($registration_id == null) {
            // try to find device form db
            $device_val = $this->dbConnector->findOne([
                'device_id' => $device_id
            ]);

            if ($device_val !== null && isset($device_val['registration_id'])) {
                $registration_id = $device_val['registration_id'];
            }
        }
        $this->entity = new DeviceEntity($device_id, $registration_id);
        return $this->entity;
    }

    public function addDevice()
    {

        $this->dbConnector->insert($this->entity, array('upsert' => true));
    }

}