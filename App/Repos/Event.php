<?php
/**
 * Created by PhpStorm.
 * User: engin
 * Date: 18/04/15
 * Time: 10:37
 */

namespace App\Repos;

use App\Entity\Event as EventEntity;
use App\Entity\User;

/**
 * Class Event
 * @package App\Repos
 */
class Event
{
    /** contains db connection
     * @var
     */
    private $dbConnector;
    /** contains mongo collection name of object
     * @var string
     */
    protected $collection = 'events';
    /** contains entity object
     * @var EventEntity
     */
    public $entity;


    /**
     * @param $dbConnectionClient
     */
    public function  __construct($dbConnectionClient)
    {
        $this->dbConnector = $dbConnectionClient;
        $this->dbConnector = $this->dbConnector->selectCollection('sma', $this->collection);
    }

    /** initializes new event entity if there is an existsing event with given id nor populates values,
     * @param $id
     * @return $this
     */
    public function init($id)
    {
        $this->entity = new EventEntity();
        $event_values = null;
        if (!empty($id)) {
            // existing event
            $event_values = $this->dbConnector->findOne([
                '_id' => new \MongoId($id)
            ]);
        }

        if ($event_values !== null) {
            foreach ($event_values as $prop => $value) {
                $this->entity->{$prop} = $value;
            }
        }

        return $this;
    }

    /** return event entity object
     * @return EventEntity
     */
    public function getObject()
    {
        return $this->entity;
    }

    /** adds new message to event with given user
     * @param User $user
     * @param $message
     */
    public function addMessage(User $user, $message)
    {
        $this->entity->addMessage($user, $message);
    }

    /** adds new comment to event with given user
     * @param User $user
     * @param $comment
     */
    public function addComment(User $user, $comment)
    {
        $this->entity->addComment($user, $comment);
    }

    /** sets event requester
     * @param User $user
     */
    public function setRequester(User $user)
    {
        $this->entity->requester = $user;
    }

    /** sets event responder
     * @param User $user
     */
    public function setResponder(User $user)
    {
        $this->entity->responder = $user;
    }

    /** sets location of event
     * @param $location
     */
    public function setLocation($location)
    {
        $this->entity->location = $location;
    }

    /** gets status of the event
     * @return string
     */
    public function getStatus()
    {
        return $this->entity->status;
    }

    /** sets status of the event
     * @param $status
     */
    public function setStatus($status)
    {
        $this->entity->status = $status;
    }

    /** returns all events of user
     * @param $userid
     * @return mixed
     */
    public function getUserEvents($userid)
    {
        return $this->dbConnector->find([
            '$or' =>
                ["requester._id" => new \MongoId($userid)],
            ["responder._id" => new \MongoId($userid)]
        ]);
    }

    /** returns latest 5 events of user
     * @param $userid
     * @return mixed
     */
    public function getLatestEvents($userid)
    {
        return $this->dbConnector->find([
            '$or' =>
                ["requester._id" => new \MongoId($userid)],
            ["responder._id" => new \MongoId($userid)]
        ])->sort(['approve_date' => -1])->limit(5);
    }

    /**
     * if entity has already exist in db updates, persist entity otherwise
     */
    public function add()
    {
        if (isset($this->entity->_id)) {
            $this->dbConnector->update(['_id' => $this->entity->_id], $this->entity, array('fsync' => true));
        } else {
            $this->dbConnector->insert($this->entity, array('upsert' => true));
        }
    }
}