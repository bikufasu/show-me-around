<?php
/**
 * Created by PhpStorm.
 * User: mvelioglu
 * Date: 03/04/15
 * Time: 00:05
 */

namespace App\Repos;

use App\Entity\Event;
use App\Entity\User as UserEntity;
use App\Libs\Util;
use App\Repos\Event as EventRepo;

class User
{
    private $dbConnector;
    private $dbConnection;
    protected $collection = 'user';
    public $user;


    public function  __construct($dbConnectionClient)
    {
        $this->dbConnection = $dbConnectionClient;
        $this->dbConnector = $this->dbConnection->selectCollection('sma', $this->collection);
    }

    public function init($id)
    {
        $values = null;
        if (empty($id)) {
            $id = Util::generate_session_id();
            $this->user = new UserEntity($id);
        } else {
            $values = $this->dbConnector->findOne([
                'session_id' => $id
            ]);

            $this->user = new UserEntity($values['session_id']);

            // get events
            $eventRepo = new EventRepo($this->dbConnection);
            $this->user->events = $eventRepo->getUserEvents((string)$values['_id']);
        }
        $this->user->session_id = $id;
        if ($values !== null) {
            foreach ($values as $prop => $value) {
                $this->user->{$prop} = $value;
            }
        }

        return $this;
    }

    public function getObject()
    {
        return $this->user;
    }

    public function setLocation($lat, $long)
    {
        $this->user->location = [doubleval($lat), doubleval($long)];
    }

    public function getLocation()
    {
        return $this->user->location;
    }

    public function setName($name)
    {
        $this->user->name = $name;
    }

    public function setPhotoLink($link)
    {
        $this->user->photo_link = $link;
    }

    public function setFacebookInfo($fbinfo)
    {
        $this->user->social_accounts['facebook'] = $fbinfo;
    }

    public function addUser()
    {
        $this->user->updated_at = new \DateTime('now');
        if (isset($this->user->_id)) {
            $this->dbConnector->update(['_id' => $this->user->_id], $this->user, array('fsync' => true));
        } else {
            $this->dbConnector->insert($this->user, array('upsert' => true));
        }

    }

}