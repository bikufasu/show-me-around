<?php
/**
 * Created by PhpStorm.
 * User: engin
 * Date: 19/04/15
 * Time: 09:55
 */

namespace App\Repos;

use App\Entity\Location as LocationEntity;

class Location
{

    private $dbConnector;
    protected $collection = 'user';
    public $entity;


    public function  __construct($dbConnectionClient)
    {
        $this->dbConnector = $dbConnectionClient;
        $this->dbConnector = $this->dbConnector->selectCollection('sma', $this->collection);
    }

    public function init($lat, $long)
    {
        $this->entity = new LocationEntity($lat, $long);
        return $this;
    }

    public function getNearUsers($radius)
    {
        $results = $this->dbConnector->find([
            'location' => [
                '$geoWithin' => [
                    '$center' => [
                        [$this->entity->lat, $this->entity->long],
                        $radius
                    ]
                ]
            ]
        ]);

        $response = [];
        foreach ($results as $result) {
            $response[] = $result;
        }

        return $response;
    }
}