<?php

/**
 * Created by PhpStorm.
 * User: mvelioglu
 * Date: 03/04/15
 * Time: 03:46
 */
namespace App\Libs;

use Facebook\FacebookAuthorizationException;
use Facebook\FacebookRequestException;
use Facebook\FacebookSession;
use Facebook\FacebookRequest;

class Util
{

    /**
     * Generates random session id with mixed characters
     * @return string
     */
    public static function generate_session_id()
    {

        $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 35; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode('', $pass);
    }

    public static function getInfoFromFacebook($access_token)
    {
        try {
            return (new FacebookRequest(new FacebookSession($access_token),
                'GET',
                '/me',
                [
                    'access_token' => $access_token
                ]))->execute()->getRawResponse();
        } catch (FacebookAuthorizationException $auth_exception) {
            // -
        } catch (FacebookRequestException $request_exception) {
            // -
        } catch (\Exception $ex) {
            // -
        }
    }

}