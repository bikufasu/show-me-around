<?php
/**
 * Project: show-me-around
 * Author: Mehmet Ali Ergut ( memnuniyetsizim )
 * Date: 15/03/15
 * Time: 20:34
 */
use \Symfony\Component\HttpFoundation;
use App\Model\Error;

$app->mount('/user', new \App\Controller\UserController());
$app->mount('/event', new \App\Controller\EventController());
$app->mount('/location', new \App\Controller\LocationController());

$app->error(function (Error $e) use ($app) {
    $response = $e->get($e->getCode());
    return $app->json([
        'status' => $response['status'],
        'message' => $response['message'],
        'code' => $response['code']
    ], $response['http_status']);
});

$app->before(function (HttpFoundation\Request $request) use ($app) {

    if ($request->headers->get('X-USER-DEVICEID', '') === '') {
        throw new Error("Device Id header not found", 10000);
    }

    $session_id = $request->headers->get('X-SESSIONID', '');
    $app['logged_user'] = $app->share(function () use ($app, $session_id) {
        return $app['model.provider']->userProvider($session_id);
    });
});


$app->after(function (HttpFoundation\Request $req, HttpFoundation\Response $r) use ($app) {
    if ($r instanceof HttpFoundation\RedirectResponse === false) {
        $app['logged_user']->addUser();
    }

});

$app['mongodb'] = $app->share(function () use ($app) {
    return $app['mongo'];
});

$app['model.provider'] = $app->share(function () use ($app) {
    return new App\Model\Provider($app);
});