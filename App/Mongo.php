<?php
/**
 * Created by PhpStorm.
 * User: engin
 * Date: 22/03/15
 * Time: 20:09
 */

namespace App;


use Silex\Application;

trait Mongo
{

    public function __call($method, $args)
    {
        global $app;
        $mth = new \ReflectionMethod(get_class($app['mongodb']), $method);

        return $mth->invokeArgs($app['mongodb'], $args);
    }
}